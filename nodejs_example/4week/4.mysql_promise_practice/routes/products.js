var express = require('express');
var router = express.Router();
const products = require('../models/products')
const db = require('../components/db')

router.get('/', async function(req, res, next) {
    try {
        const {idx} = req.query
        const connection = await db.getConnection()
        const results = await products.getList(connection,{idx:idx}) //a query
        res.status(200).json({results})         
    } catch(err){
        console.log('products get error : ',err)
        next()
    }   
})

router.post('/',async function (req, res, next) {
    const body = req.body; // {name:asdf,price:200}    
    try {        
        const connection = await db.getConnection()
        await db.beginTransaction(connection)
        const result = await products.insert(connection,body)
        await db.commit(connection)
        res.status(200).json({result})         
    } catch (err){
        next()        
    } 
})

router.put('/',async function (req, res) {
    const body = req.body  // { "idx":3,"name":"changed","price":12320000}
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const result = await products.update(connection,body)
    await db.commit(connection)
    res.status(200).json({result})    
})

router.delete('/',async function (req, res) {
    const body = req.body  
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const result = await products.delete(connection,body)
    await db.commit(connection)
    res.status(200).json({result})    
})


module.exports = router;
