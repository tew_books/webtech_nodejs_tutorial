
module.exports.getList = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            `SELECT * FROM products`, 
            function (error, results, fields) {
                // connection.release() // query 조합시 삭제해야 함
                if (error) {
                    reject(error)
                }
                resolve(results)
            }) 
    })    
}

module.exports.insert = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'INSERT INTO products SET ?', 
            options,
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.update = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'UPDATE products SET ? WHERE idx = ?', 
            [options, options.idx], 
            function (error, result, fields) {
                if (error) {
                    // connection.release()  
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.delete = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'DELETE FROM products WHERE idx = ?', 
            options.idx, 
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}