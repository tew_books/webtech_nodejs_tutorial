var express = require('express');
var router = express.Router();
const users = require('../models/users')
const db = require('../components/db')

router.get('/', async function(req, res, next) {
    try {
        const {user_idx} = req.query
        const connection = await db.getConnection()
        const results = await users.getList(
                connection,
                {user_idx:user_idx} // options
            ) //a query
        res.status(200).json({results})         
    } catch(err){
        console.log('users get error : ',err)
        next()
    }   
})

router.post('/signup',async function (req, res, next) {
    const body = req.body; // {userId:asdf,pwd:sdfsd}    
    try {        
        const connection = await db.getConnection()
        await db.beginTransaction(connection)
        const userList = await users.getList(connection, {userId : body.userId}) 
        if (userList.length > 0){ // userList = [] 
            return res.status(200).json({result:'user exist'})         
        }
        const result = await users.insert(connection,body)
        await db.commit(connection)
        res.status(200).json({result})         
    } catch (err){
        console.log('signup err : ',err)
        next()        
    } 
})

router.post('/signin',async function (req, res, next) {
    const body = req.body; // {userId:asdf,pwd:sdfsd}  
    try {        
        // 아이디와 pwd가 존재 할 때 true   
        const connection = await db.getConnection()
        const userList = await users.getList(connection, body)
        if (userList.length > 0){
            res.status(200).json({result:true})         
        } else {
            res.status(200).json({result:false})         
        }
    } catch (err){
        next()        
    } 
})

router.put('/',async function (req, res) {
    try {
        const body = req.body  // {user_idx :3, userId:sdf,pwd:adsfs}
        const connection = await db.getConnection()
        await db.beginTransaction(connection)
        const result = await users.update(connection,body)
        await db.commit(connection)
        res.status(200).json({result})    
    } catch (err){

        next()        
    }     
})

router.delete('/',async function (req, res) {
    try {
        const body = req.body  
        const connection = await db.getConnection()
        await db.beginTransaction(connection)
        const result = await users.delete(connection,body)
        await db.commit(connection)
        res.status(200).json({result})    
    } catch (err){
        
        next()        
    } 
})



module.exports = router;
