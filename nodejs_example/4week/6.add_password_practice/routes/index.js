var express = require('express');
var router = express.Router();

const mysql      = require('mysql');
// const connection = mysql.createConnection({
//   host     : '127.0.0.1',
//   user     : 'root',
//   password : '12341234'
// });
const pool  = mysql.createPool({
  connectionLimit : 10,
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234',
  database : 'sys'
});

/* GET home page. */
router.get('/', function(req, res, next) {
    // mysql db 연결 1
    pool.getConnection(function(err, connection) {
      if (err) throw err // not connected!
      connection.query(`SELECT name AS pd_name FROM products  LIMIT 1, 1`, 
      function (error, results, fields) {// When done with the connection, release it.
        connection.release()
        console.log('getConnection : ',results)
        // Handle error after the release.
        if (error) throw error
        // Don't use the connection here, it has been returned to the pool.
        res.status(200).json({results})        
      })
  })
})


module.exports = router;
