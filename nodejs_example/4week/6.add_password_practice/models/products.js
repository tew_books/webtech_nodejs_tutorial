
const db = require('../components/db')

module.exports.getList = async(
    connection, // PoolConnection
    options // {idx, name}
    ) => { 
    let query = `SELECT * FROM products`
    let values = []
    if (options){
        if (options.idx) {
            query += ' WHERE idx = ?'
            values.push(options.idx)
        }
    }    
    return await db.query(connection, {
        query:query,
        values:values
    })
}

module.exports.insert = async(connection, options) => {
    let query = `INSERT INTO products SET ?`
    let values = options
    return await db.query(connection, {
        query:query,
        values:values
    })
}

module.exports.update = async(connection, options) => {
    let query = `UPDATE products SET ? WHERE idx = ?`
    let values = [options, options.idx]
    return await db.query(connection, {
        query:query,
        values:values
    })    
}

module.exports.delete = async(connection, options) => {
    let query = `DELETE FROM products WHERE idx = ?`
    let values = options.idx
    return await db.query(connection, {
        query:query,
        values:values
    })        
}