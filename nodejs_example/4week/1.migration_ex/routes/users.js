var express = require('express');
var router = express.Router();
const fs = require('fs');
const users = require('../models/users')
const db = require('../components/db')
const crypto = require('../components/crypto')

router.post('/signup', async function(req, res, next) {  
  const user = req.body
  try {
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const usreResult = await users.getList(connection, {id:user.id})
    if (usreResult.length > 0) {
        res.status(201).json({result:'User already exist'})
        return;
    }    

    const  {encodedPw, salt} = await crypto.createPasswordPbkdf2(user.pwd)
    user.pwd = encodedPw
    user.salt = salt
    const result = await users.insert(connection, user)
    await db.commit(connection)    
    res.status(200).json({result})
  } catch (err) {
    console.log('err : ', err)
    next()
  }
  
});


router.post('/signin', async function(req, res, next) {  
  const user = req.body
  try {
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const usreResult = await users.getList(connection, user)
    if (usreResult.length < 1) {
        res.status(200).json({result:'User not exist'})
        return;
    }        
    const salt = usreResult[0].salt
    const pwd = user.pwd
    const checkPassword = crypto.getPasswordPbkdf2(pwd, salt)
    if (usreResult[0].pwd == checkPassword) {
        res.status(200).json({result:true})
    } else {
        res.status(200).json({result:false})
    }

  } catch (err) {
    console.log('err : ', err)
    next()
  }
});

router.get('/', async function(req, res, next) {
    try {
        const connection = await db.getConnection()    
        const result = await users.getList(connection, {})
        res.status(200).json({result})        
    } catch (err) {
        console.log(err)
        next()
    }  
});

router.put('/', async function(req, res, next) {
    try {
      const result = await users.update(req.body)
      res.status(200).json({result})
    } catch (err) {
      console.log('err : ', err)
      next()
    }
  });

  
module.exports = router;
