var express = require('express');
var router = express.Router();

const mysql      = require('mysql');
// const connection = mysql.createConnection({
//   host     : '127.0.0.1',
//   user     : 'root',
//   password : '12341234'
// });
const pool  = mysql.createPool({
  connectionLimit : 10,
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234',
  database : 'sys'
});


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
