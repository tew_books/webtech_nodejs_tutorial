var express = require('express');
var router = express.Router();
const products = require('../models/products')

router.post('/', async function(req, res, next) {
  try {
    const result = await products.insert(req.body)
    res.status(200).json({result})
  } catch (err) {
    console.log('err : ', err)
    next()
  }
});

router.put('/', async function(req, res, next) {
  try {
    const result = await products.update(req.body)
    res.status(200).json({result})
  } catch (err) {
    console.log('err : ', err)
    next()
  }
});

router.delete('/',async function(req, res, next) {  
  try {
    const result = await products.delete(req.body)
    res.status(200).json({result})
  } catch (err) {
    console.log('err : ', err)
    next()
  }
});

/* GET home page. */
router.get('/',async function(req, res, next) {
  const {products_idx} = req.query
  try {
    const result = await products.getList({products_idx})
    res.status(200).json({result})        
  } catch (err) {
    console.log(err)
    next()
  }  
});

module.exports = router;
