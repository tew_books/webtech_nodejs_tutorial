var express = require('express');
var router = express.Router();
const formidable = require('formidable')
const path = require('path')
const fs = require('fs')
const usersImg = require('../models/usersImg')
const users = require('../models/users')
const db = require('../components/db')


router.post('/', async function(req, res, next) {
    try {
        const connection = await db.getConnection()
        await db.beginTransaction(connection)
        const usreResult = await users.getList(connection, {users_idx:req.body.users_idx})
        if (usreResult.length < 1) {
            res.status(200).json({result:'User not exist'})
            return;
        }        
        const result = await usersImg.insert(connection, req.body)
        await db.commit(connection)    
        res.status(200).json({result})
    } catch (err) {
      console.log('err : ', err)
      next()
    }
  });

  
router.post('/upload',async function (req, res, next) {
    try {        
        const form = formidable({ multiples: true });
        form.parse(req, (err, fields, files) => {
            if (err) {
                return res.json({ result: 'upload fail'});
            }
            
            const file = files.image
            console.log('file : ',file)
            const dir = `public/images/${3}`
            !fs.existsSync(dir) && fs.mkdirSync(dir)
            const newPath = path.join(__dirname, '..', `${dir}/${file.originalFilename}`)
            console.log('newPath : ', newPath)
            console.log('file.path : ', file.filepath)
            // fs.renameSync
            fs.renameSync(file.filepath, newPath)  //경로를 바꿔줍니다.
            res.json({ result: `images/${3}/${file.originalFilename}`});
        })         
    } catch (err){
        console.log('signin err : ',err)
        next()        
    } 
})


module.exports = router;