const mysql      = require('mysql');
const pool  = mysql.createPool({
  connectionLimit : 10,
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234',
  database : 'sys'
});


module.exports.getConnection = () => {  
  return new Promise((resolve, reject) => {
    pool.getConnection(function(err, connection) {
      if (err) reject(err)
      resolve(connection)
    })
  })
}


module.exports.beginTransaction = (connection) => {  
  return new Promise((resolve, reject) => {
    connection.beginTransaction(function(err) {
      if (err) { 
          reject(err)
      }    
      resolve()     
    })
  })
}

module.exports.query = (connection, options) => {  
  const {sql, values} = options
  return new Promise((resolve, reject) => {
    connection.query(sql, 
    values,    
    function (error, result, fields) {// When done with the connection, release it.
      if (error) {    
        connection.rollback(function() {                                    
          reject(error)
        })              
      }       
      resolve(result)      
    })
  })
}

module.exports.commit = (connection) => {  
  return new Promise((resolve, reject) => {
    connection.commit(function(err) {      
      if (err) {
          connection.rollback(function() {
            reject(error)
          })
      }
      connection.release()
      resolve()
    })       
  })
}