
const mysql      = require('mysql');
const db = require('../components/db')


module.exports.insert = async(connection, options) => {      
    const sql = `INSERT INTO user_images SET ?`
    const values = options
    const result = await db.query(connection, {sql, values})
    return result  
}


module.exports.update = async(options) => {    
  const connection = await db.getConnection()    
  await db.beginTransaction(connection)
  const sql = `UPDATE user_images SET ?
              WHERE user_images_idx = ?`
  const values = [options, options.user_images_idx]
  const result = await db.query(connection, {sql, values})  
  await db.commit(connection)  
  return result  
}

module.exports.delete = async(options) => {    
    const connection = await db.getConnection()    
    await db.beginTransaction(connection)
    const sql = ` DELETE FROM user_images
                  WHERE user_images_idx = ?`
    const values = options.user_images_idx
    const result = await db.query(connection, {sql, values})   
    await db.commit(connection) 
    return result
}

module.exports.getList = async (connection, options) => {  
    const {user_images_idx, id} = options
    let values;
    let sql = `SELECT * FROM user_images `
    if(user_images_idx) { 
      sql += ' WHERE user_images_idx = ?'
      values = user_images_idx
    } 
    const result = await db.query(connection, {sql, values})    
    return result;  
}