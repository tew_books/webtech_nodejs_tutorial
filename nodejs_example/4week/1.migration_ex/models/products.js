
const mysql      = require('mysql');
const db = require('../components/db')


module.exports.insert = async(options) => {  
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const sql = `INSERT INTO products SET ?`
    const values = options
    const result = await db.query(connection, {sql, values})
    await db.commit(connection)
    return result  
}


module.exports.update = async(options) => {    
  const connection = await db.getConnection()    
  await db.beginTransaction(connection)
  const sql = `UPDATE products SET ?
              WHERE products_idx = ?`
  const values = [options, options.products_idx]
  const result = await db.query(connection, {sql, values})  
  await db.commit(connection)  
  return result  
}

module.exports.delete = async(options) => {    
    const connection = await db.getConnection()    
    await db.beginTransaction(connection)
    const sql = ` DELETE FROM products
                  WHERE products_idx = ?`
    const values = options.products_idx
    const result = await db.query(connection, {sql, values})   
    await db.commit(connection) 
    return result
}

module.exports.getList = async (options) => {  
    const {products_idx} = options
    let sql = `SELECT * FROM products `
    if(products_idx) { 
      sql += ' WHERE products_idx = ?'
    }
    const connection = await db.getConnection()    
    const values = products_idx
    const result = await db.query(connection, {sql, values})    
    connection.release()
    return result;  
}