
const mysql      = require('mysql');
const db = require('../components/db')


module.exports.insert = async(connection, options) => {      
    const sql = `INSERT INTO users SET ?`
    const values = options
    const result = await db.query(connection, {sql, values})
    return result  
}


module.exports.update = async(options) => {    
  const connection = await db.getConnection()    
  await db.beginTransaction(connection)
  const sql = `UPDATE users SET ?
              WHERE users_idx = ?`
  const values = [options, options.users_idx]
  const result = await db.query(connection, {sql, values})  
  await db.commit(connection)  
  return result  
}

module.exports.delete = async(options) => {    
    const connection = await db.getConnection()    
    await db.beginTransaction(connection)
    const sql = ` DELETE FROM users
                  WHERE users_idx = ?`
    const values = options.users_idx
    const result = await db.query(connection, {sql, values})   
    await db.commit(connection) 
    return result
}

module.exports.getList = async (connection, options) => {  
    const {users_idx, id} = options
    let values;
    let sql = `SELECT * FROM users
               LEFT JOIN user_images ON user_images.users_idx = users.users_idx`
    if(users_idx) { 
      sql += ' WHERE users.users_idx = ?'
      values = users_idx
    } else if(id) { 
      sql += ' WHERE users.id = ?'
      values = id
    }
    const result = await db.query(connection, {sql, values})    
    return result;  
}