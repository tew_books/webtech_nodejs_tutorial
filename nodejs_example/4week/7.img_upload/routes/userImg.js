var express = require('express');
var router = express.Router();
const users = require('../models/users')
const db = require('../components/db')
const crypto = require('../components/crypto')

const formidable = require('formidable')
const path = require('path')
const fs = require('fs')

router.post('/upload',async function (req, res, next) {
    try {        
        const form = formidable({ multiples: true });
        form.parse(req, (err, fields, files) => {
            if (err) {
                return res.json({ result: 'upload fail'});
            }
            console.log('files : ',files)
            const file = files.image
            const dir = `public/images/${3}`
            !fs.existsSync(dir) && fs.mkdirSync(dir)
            const newPath = path.join(__dirname, '..', `${dir}/${file.name}`)
            fs.renameSync(file.path, newPath)  //경로를 바꿔줍니다.
            res.json({ result: `images/${3}/${file.name}`});
        })         
    } catch (err){
        console.log('signin err : ',err)
        next()        
    } 
})

router.put('/upload',async function (req, res, next) {
    try {
        const {imgPath} = req.query
        const form = formidable({ multiples: true });

        form.parse(req, (err, fields, files) => {
            if (err) {
                return res.json({ result: 'upload fail'});
            }
            const file = files.image
            const dir = `public/${imgPath}`
            console.log('imgPath : ',imgPath)
            const newPath = path.join(__dirname,'..', `${dir}`)
            fs.renameSync(file.path, newPath)  //경로를 바꿔줍니다.
            res.json({ result: true});
        })    
    } catch (err){
        console.log('err : ',err)
        next(err)
    }
})



module.exports = router;
