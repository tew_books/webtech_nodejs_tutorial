var express = require('express');
var router = express.Router();

const mysql      = require('mysql');
// const connection = mysql.createConnection({
//   host     : '127.0.0.1',
//   user     : 'root',
//   password : '12341234'
// });
const pool  = mysql.createPool({
  connectionLimit : 10,
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234',
  database : 'sys'
});

/* GET home page. */
router.get('/', function(req, res, next) {
    // mysql db 연결 1
    pool.getConnection(function(err, connection) {
      if (err) throw err // not connected!
      connection.query(`SELECT * FROM products`, 
      function (error, results, fields) {// When done with the connection, release it.
        connection.release()
        console.log('getConnection : ',results)
        // Handle error after the release.
        if (error) throw error
        // Don't use the connection here, it has been returned to the pool.
        res.status(200).json({results})        
      })
  })
})


router.post('/', function (req, res, next) {
  const body = req.body; // {name:asdf,price:200}
  console.log('body : ', body)
  pool.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      connection.beginTransaction(function(err) {
          if (err) { 
              res.status(200).json({error:err})                
          }            
          connection.query(
              'INSERT INTO products SET ?', 
              body,
              function (error, result, fields) {
                  if (error) {
                      connection.release()
                      return connection.rollback(function() {                                    
                          res.status(200).json({error:error})                            
                      })
                  } 
                  connection.commit(function(err) {
                      connection.release()
                      if (err) {
                          return connection.rollback(function() {
                            res.status(200).json({err:err})                                
                          })
                      }
                      res.status(200).json({result})
                  })                       
          })          
      })
  })
})

// router.put('/', function(req, res, next) {
//   const {name, idx } = req.body
//   pool.getConnection(function(err, connection) {
//     if (err) throw err // not connected!
//     connection.query(`
//       UPDATE products SET name = '${name}'
//       WHERE idx = ${idx}
//     `, 
//     function (error, results, fields) {// When done with the connection, release it.
//       connection.release()
//       console.log('getConnection : ',results)
//       if (error) throw error      
//       res.status(200).json({results})        
//     })
//   })
// })

router.put('/', function (req, res) {
  const body = req.body  // { "idx":3,"name":"changed","price":12320000}
  pool.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      connection.beginTransaction(function(err) {
          if (err) { 
              res.status(200).json({error:err})                
          }
          connection.query(
              'UPDATE products SET ? WHERE idx = ?', 
              [body, body.idx], 
              function (error, result, fields) {
                  if (error) {
                      connection.release();
                      return connection.rollback(function() {
                          res.status(200).json({error:error})                            
                      })
                  }
                  connection.commit(function(err) {
                      connection.release();
                      if (err) {
                          return connection.rollback(function() {
                            res.status(200).json({error:err})                                
                          })
                      }
                      res.status(200).json({result})
                  })   
              }
          )
      })
  })    
})

// router.delete('/', function(req, res, next) {
//   const { idx } = req.body
//   pool.getConnection(function(err, connection) {
//     if (err) throw err // not connected!
//     connection.query(`
//       DELETE FROM products
//       WHERE idx = ${idx}
//     `, 
//     function (error, results, fields) {// When done with the connection, release it.
//       connection.release()
//       console.log('getConnection : ',results)
//       if (error) throw error      
//       res.status(200).json({results})        
//     })
//   })
// })

router.delete('/', function (req, res) {
  const body = req.body  
  pool.getConnection(function(err, connection) {
      if (err) throw err; // not connected!
      connection.beginTransaction(function(err) {
          if (err) { 
              res.status(200).json({error:err})                
          }
          connection.query(
              'DELETE FROM products WHERE idx = ?', 
              body.idx, 
              function (error, result, fields) {
                  if (error) {
                      connection.release()
                      return connection.rollback(function() {
                          res.status(200).json({error:error})                            
                      })
                  }
                  connection.commit(function(err) {
                      connection.release()
                      if (err) {
                          return connection.rollback(function() {
                            res.status(200).json({error:err})                                
                          })
                      }
                      res.status(200).json({result})
                  })
              }
          )
      })
  })      
})


module.exports = router;
