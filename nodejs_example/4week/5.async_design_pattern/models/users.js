
module.exports.getList = (connection, options) => {
    return new Promise((resolve, reject) => {
        let query = 'SELECT * FROM users'
        let values = []
        if(options){
            if(options.userId){
                query+= ' WHERE userId = ?'
                values.push(options.userId)
                if(options.pwd){
                    query+= ' AND pwd = ?'
                    values.push(options.pwd)
                }
            }
            
        }
        console.log('query : ',query)

        connection.query(
            query, 
            values,
            function (error, results, fields) {
                // connection.release() query 조합시 최종에 해야 함
                if (error) {
                    reject(error)
                }
                resolve(results)
            }) 
    })    
}

module.exports.insert = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'INSERT INTO users SET ?', 
            options,
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.update = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'UPDATE users SET ? WHERE user_idx = ?', 
            [options, options.user_idx], 
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.delete = (connection, options) => {
    return new Promise((resolve, reject) => {
        connection.query(
            'DELETE FROM users WHERE user_idx = ?', 
            options.user_idx, 
            function (error, result, fields) {
                if (error) {
                    connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}