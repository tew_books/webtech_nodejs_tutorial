
const db = require('../components/db')

module.exports.getList = async(connection, options) => {

    return await db.query(
        connection,
        {
            query: 'SELECT * FROM products',
            value: []
        }
    ) 

    // return new Promise((resolve, reject) => {
    //     connection.query(
    //         `SELECT * FROM products`, 
    //         function (error, results, fields) {
    //             // connection.release() // query 조합시 삭제해야 함
    //             if (error) {
    //                 reject(error)
    //             }
    //             resolve(results)
    //         }) 
    // })    
}

module.exports.insert = async(connection, options) => {
    return await db.query(
        connection,
        {
            query: 'INSERT INTO products SET ?',
            value: options
        }
    ) 
    return new Promise((resolve, reject) => {
        connection.query(
            'INSERT INTO products SET ?', 
            options,
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.update = async(connection, options) => {
    return await db.query(
        connection,
        {
            query: 'UPDATE products SET ? WHERE idx = ?',
            value: [options, options.idx]
        }
    ) 
    return new Promise((resolve, reject) => {
        connection.query(
            'UPDATE products SET ? WHERE idx = ?', 
            [options, options.idx], 
            function (error, result, fields) {
                if (error) {
                    // connection.release()  
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}

module.exports.delete = async(connection, options) => {
    return await db.query(
        connection,
        {
            query: 'DELETE FROM products WHERE idx = ?',
            value: [options.idx]
        }
    ) 
    return new Promise((resolve, reject) => {
        connection.query(
            'DELETE FROM products WHERE idx = ?', 
            options.idx, 
            function (error, result, fields) {
                if (error) {
                    // connection.release()
                    reject(error)
                } 
            resolve(result)                    
        })   
    })    
}