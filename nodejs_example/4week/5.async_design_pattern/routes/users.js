var express = require('express');
var router = express.Router();
const users = require('../models/users')
const db = require('../components/db')

router.get('/', async function(req, res, next) {
    try {
        const {idx} = req.query
        const connection = await db.getConnection()
        const results = await users.getList(connection,{idx:idx}) //a query
        res.status(200).json({results})         
    } catch(err){
        console.log('products get error : ',err)
        next()
    }   
})

router.post('/signup',async function (req, res, next) {
    const body = req.body; // {name:asdf,price:200}    
    try {        
        const connection = await db.getConnection()
        // 중복 아이디 체크 
        // 중복 아이디 없으면 입력
        await db.beginTransaction(connection)
        const userList = await users.getList(connection, {userId : body.userId})
        console.log('userList : ',userList)
        if (userList.length>0){
            return res.status(200).json({result:'user exist'})         
        }

        const result = await users.insert(connection,body)
        await db.commit(connection)
        res.status(200).json({result})         
    } catch (err){
        next()        
    } 
})

router.post('/signin',async function (req, res, next) {
    const body = req.body; // {name:asdf,price:200}    
    try {        
        // 아이디와 pwd가 존재 할 때 true   
        const connection = await db.getConnection()
        const userList = await users.getList(connection, {userId : body.userId, pwd:body.pwd})
        console.log('userList : ',userList)
        if (userList.length>0){
            res.status(200).json({result:true})         
        } else {
            res.status(200).json({result:false})         
        }

    } catch (err){
        next()        
    } 
})

router.put('/',async function (req, res) {
    const body = req.body  // { "idx":3,"name":"changed","price":12320000}
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const result = await users.update(connection,body)
    await db.commit(connection)
    res.status(200).json({result})    
})

router.delete('/',async function (req, res) {
    const body = req.body  
    const connection = await db.getConnection()
    await db.beginTransaction(connection)
    const result = await users.delete(connection,body)
    await db.commit(connection)
    res.status(200).json({result})    
})


module.exports = router;
