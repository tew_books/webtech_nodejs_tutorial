// 블로킹 코드
const fs = require('fs');
const { connect } = require('http2');
// try {
//     const data = fs.readFileSync('./fille.md'); // 파일을 읽을 때까지 여기서 블로킹 됩니다.
//     // const data = fs.readFileSync('./filed.md'); // 에러 확인
//     console.log('data: ',data)
//     //위 코드는 에러 핸들링이 없음
// } catch (err) {
//     console.log('err : ',err)
// // Here you get the error when the file was not found,
// // but you also get any other error
// }


//non 블로킹 코드
//논블로킹 코드는 기다려 주지 않음으로, 작업이 완료된 후 코드 실행을 원한다면 아래와 같이 콜백 함수를 만들어야 합니다.
// fs.readFile('./file.md', (err, data) => {
//     if (err) throw err;
//     console.log('non blocking data : ',data);
// });
// console.log('wow')


// 콜백함수 1
// function callFunction(callback){
//     console.log("callFunction called")
//     let one = 1
//     let two = 2
//     let result = one+two
    
//     if(result>2){
//         callback("biggner than 2")
//     } else {
//         callback("not biggner than 2")
//     }    
// }

// callFunction(
//     function(getMsg){ // callback
//         console.log(getMsg)
//     }
// ) 

// callFunction()


// function sumFunc(num1, num2, callback){    
//     // 엄청 복잡해서 1초 후에 답을 줄 수 있음
//     setTimeout(() => {
//         callback(num1+num2)
//         return num1+num2
//     }, 1000);
// }
// const reuslt = sumFunc(5, 2, function(getMsg){ // callback
//     console.log('getMsg : ',getMsg)
// })
// console.log('reuslt : ',reuslt)


// // // 콜백함수 2
// function sumFunction(num1, num2, callback){
//     // num1과 num2를 매개변수로 받아서 더합니다.
//     let result = num1+num2
//     if('number' == typeof(result)){
//         // 결과가 숫자일 경우, 결과를 callback 함수의 매개변수로 전달합니다.
//         let error
//         let message = result
//         callback(error, message)
//     } else {
//         // 결과가 숫자가 아닐 경우, 에러를 callback 함수의 매개변수로 전달합니다.
//         let error = 'is Not Number : '
//         let message 
//         callback(error, message)
//     }
// }


// 위 함수를 실행 시 매개변수로 입력값, 콜백함수를 전달 합니다.
// sumFunction(1, 2, 
//     function(error, message){ // callback 함수
//     // 위 함수에서 내부 함수가 실행된 결과를 error와 message라는 매개함수로 전달받습니다.
//         console.log("error : ",error)
//         console.log("message : ",message)
//     }
// )

// function wakeUp() {
//     console.log('wake up!')
// }

// // arrow function
// var wakeUp =  function(){
//     console.log('wake up!')
// }
// setInterval(wakeUp, 1000);
//특정 기간동안 특정 기능을 실행 하는 함수
//wakeUp은 setInterval이라는 함수의 결과로 실행되는 'callback' 함수 입니다.

//위 함수는 아래와 같이 익명함수로 변경할 수 있습니다.
// setInterval(function(){
//     console.log('wake up!')
// }, 1000);

//그리고 이러한 익명함수는 아래와 같이 화살표 함수 표현으로 대체할 수 있습니다.
// setInterval(() => { // = function(){
//     console.log('wake up!')
// }, 1000);

// 메소드 테스트
// const wakeUp = () => {
//     console.log('arrow ')
// }
// wakeUp()
// var wakeUp =  function(){
//     console.log('wake up!')
// }


// // 콜백 헬
// setTimeout(() => {
//     console.log('sleep')
// }, 3000);
// setTimeout(() => {
//     console.log('wake up!')
// }, 1000);
// console.log('eat');


setTimeout(() => {
    console.log('sleep')
     setTimeout(() => {
        console.log('wake up!')        
        setTimeout(() => {            
            console.log('eat');
        }, 1000);    
    }, 1000);    
}, 1000);



