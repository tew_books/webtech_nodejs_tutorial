class Car {
  constructor(wheels, doors) {
    this.wheels = wheels;
    this.doors = doors;
  }
} 

Car.prototype.break = function(){
  console.log("끼익!");
}
class Truck extends Car {
  constructor(windows, wheels, doors) {
    super(wheels, doors);
  }
  getWheels() {
    return this.wheels;
  }        
}
// Truck.prototype.break = function(){
//   console.log("끼이이이익~!");
// }
let car = new Car(4, 5);
let truck = new Truck(4, 3, 2);
car.break();
truck.break();