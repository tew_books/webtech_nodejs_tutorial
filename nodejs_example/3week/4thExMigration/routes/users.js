var express = require('express');
var router = express.Router();
const fs = require('fs')

router.post('/signup', function(req, res, next) {
  const user = req.body
  console.log('user : ',user)
  let userArray = []
  let newDb
  fs.readFile('database.json', 'utf8', function(err, data) {                
      if(data){
          let originDb = JSON.parse(data)
          let originUserDb = originDb.user
          //기존에 user database가 존재하는 지 확인합니다.          
          if(originUserDb){
              userArray = Array.from(originUserDb)
              // user가 이미 가입 되어 있는 지 확인합니다.
              for(let i=0;i<userArray.length;i++){
                  let originUser = userArray[i]
                  if(user.id == originUser.id){                                   
                      res.status(201).json({result:false,msg : 'user exist'})
                      return                                        
                  } else {
                      console.log('User not exist')
                  }
              }
              // user가 이미 가입 안되어 있으면 가입을 합니다.
              userArray.push(user)
              originDb.user = userArray
              newDb = JSON.stringify(originDb)
              fs.writeFile('database.json', newDb, 'utf8', function(err) {
                  res.status(201).json({
                    result:true,list : newDb
                  })
              })
          } else { // database : o, user : x
              userArray.push(user)
              originDb.user = userArray
              newDb = JSON.stringify(originDb)
              fs.writeFile('database.json', newDb, 'utf8', function(err){
                  res.status(201).json({
                    result:true,list : newDb
                  })
              })
          }
      } else { // database : x
          newDb = JSON.stringify({user:[user]})
          fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.status(201).json({result:true,list : newDb})
          })
      }
  })   
});

router.post('/signin', function(req, res, next) {
  const user = req.body
  fs.readFile('database.json', 'utf8', function(err, data) {                         
    if(data){
        let originDb = JSON.parse(data)
        let originUserDb = originDb.user               
        //우선 기존에 유저 데이터가 있는 지를 if 문으로 체크합니다.
        if(originUserDb){
            let arrayDb = Array.from(originUserDb)
            // 0, 1번
            for(let i=0;i<arrayDb.length;i++){
                let originUser = arrayDb[i]
                console.log('originUser : ',originUser)
                if(user.id == originUser.id 
                    && user.pwd == originUser.pwd){ 
                    console.log(`${i} 번째 유저와 입력값 같음`)                                  
                    // res.end(JSON.stringify({login : true}))                                        
                    res.status(201).json({login:true})
                    return
                } else {
                    console.log(`${i} 번째 유저 입력값 다름`)                                        
                }                    
            }
            // res.end(JSON.stringify({login : false}))
            res.status(201).json({login:false})
        } else {
            //유저 데이터가 없다면 로그인에 실패합니다.
            // res.end(JSON.stringify({login : false}))
            res.status(201).json({login:false})
        }                         
    } else { // data가 없을 때
        // res.end(JSON.stringify({login : false}))
        res.status(201).json({login:false})
    }
  }) 
});


router.put('/', function(req, res, next) {
  const {index, name, id, pwd} = req.body
  const user = {name:name, id:id, pwd:pwd}
  fs.readFile('database.json', 'utf8', function(err, data) {                
    // 기존 데이터를 읽음
    let originDb = JSON.parse(data)   
    let originUserDb = originDb.user               
    let arrayDb = Array.from(originUserDb)
    // 읽어온 데이터를 수정
    arrayDb[index]  = user    
    originDb.user = arrayDb
    let newDb = JSON.stringify(originDb)  
    // 수정된 데이터를 저장
    fs.writeFile('database.json', newDb, 'utf8', function(err) {
      res.status(201).json({result : true, list : arrayDb})          
    })
  })   
})
router.delete('/', function(req, res, next) {
  const {index} = req.body
  fs.readFile('database.json', 'utf8', function(err, data) {                
      let originDb = JSON.parse(data)   
      let originUserDb = originDb.user               
      let arrayDb = Array.from(originUserDb)
      delete arrayDb[index] 
      arrayDb = arrayDb.filter(x => x != null)  
      originDb.user =  arrayDb // product delete에도 추가
      let newDb = JSON.stringify(originDb)
      fs.writeFile('database.json', newDb, 'utf8', function(err) {
          // res.statusCode = 201
          // res.end(JSON.stringify({result : true, list : arrayDb}))
          res.status(201).json({result : true, list : arrayDb})          
      })
  })
});

router.get('/', function(req, res, next) {
  fs.readFile('database.json', 'utf8', function(err, data) {                
    res.status(200).json({result:true,list : data})
  })
});

module.exports = router;
