var express = require('express');
var router = express.Router();
const fs = require('fs')

/* GET poducts listing. */
router.post('/', function(req, res, next) {
    const product = req.body
    let productArray = [product]
    let newDb
    fs.readFile('database.json', 'utf8', function(err, data) {                
        if(data){
            let originDb = JSON.parse(data)                        
            console.log('originDb : ',originDb)
            //점 표기법으로 기존의 데이터를 불러옵니다.
            let originProductObc = 
                originDb.products ? originDb.products : []                        
            let arrayDb = Array.from(originProductObc)
            arrayDb.push(product)
            originDb.products = arrayDb      
            console.log('originDb : ',originDb)   
            newDb = JSON.stringify(originDb)                                                             
        } else { // no data exists                                              
            newDb = JSON.stringify({products: productArray})
        }
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.status(201).json({result:true,list : newDb})
        })
    })      
    // res.send('post request response edit');
});

router.put('/', function(req, res, next) {
    // req.body =  {
    //     "name":"상품테스트ㅅㅜ정",
    //     "price":15000,
    //     "index":0
    // }
    const {index, name, price} = req.body
    const product = {name:name, price:price}
    fs.readFile('database.json', 'utf8', function(err, data) {                
        let originDb = JSON.parse(data)   
        let originProductObc = originDb.products
        let arrayDb = Array.from(originProductObc)
        arrayDb[index]  = product    
        originDb.products = arrayDb                      
        let newDb = JSON.stringify(originDb)             
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.status(201).json({result:true,list : newDb})
        })
    }) 
});

router.delete('/', function(req, res, next) {
    const {index} = req.body
    fs.readFile('database.json', 'utf8', function(err, data) {                
        let originDb = JSON.parse(data)   
        let originProductObc = originDb.products
        let arrayDb = Array.from(originProductObc)
        delete arrayDb[index] 
        arrayDb = arrayDb.filter(x => x != null)                       
        originDb.products = arrayDb
        let newDb = JSON.stringify(originDb)
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.status(201).json({result:true,list : newDb})
        })
    })
});

router.get('/', function(req, res, next) {
    fs.readFile('database.json', 'utf8', function(err, data) {                
        res.status(200).json({result:true,list : data})
    })
});

module.exports = router;
