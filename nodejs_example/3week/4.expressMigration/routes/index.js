var express = require('express');
var router = express.Router();
const fs = require('fs')


router.post('/', function(req, res, next) {     
  // console.log("post called : ", req)        
  const good = req.body
  // console.log('good : ',good)
  // const name = good.name
  // const price = good.price
  const { 
    name,  // 상품 이름
    price  // 상품 가격
  } = req.body
  // console.log('name : ',name)
  // const good = new Good(json.name, json.price)
  let newDb
  fs.readFile('database.json', 'utf8', function(err, data) {   
      console.log('data : ', data)             
      if(data){ // 값이 없을 경우 false          
          let originDb = JSON.parse(data) 
          originDb.push(good)          
          newDb = JSON.stringify(originDb)          
      } else {
          // no data exists
          let goodsArrays = [good]                    
          newDb = JSON.stringify(goodsArrays)
      }                
      fs.writeFile('database.json', newDb, 'utf8', function(err) {
          // res.statusCode = 201
          // res.end(JSON.stringify({result : true, list : newDb}))
          res.status(201).json({result:true,list : newDb})
      })
  })         
  
});

router.put('/', function(req, res, next) {
  const good = req.body  
  fs.readFile('database.json', 'utf8', function(err, data) {                
    if(data){                
      let originDb = JSON.parse(data)    

      originDb[good.index] = {
                                name : good.name,
                                price : good.price
                              } 
      let newDb = JSON.stringify(originDb) // json string data
      fs.writeFile('database.json', newDb, 'utf8', 
          function(err) {
            res.status(201).json({result:true,list : newDb})
          })
    } else {
        // res.end('no data')
        res.status(201).json({result:false,list : []})
    }                
  })
});

router.delete('/', function(req, res, next) {
  const { index } = req.body
  fs.readFile('database.json', 'utf8', function(err, data) {                
    let originDb = JSON.parse(data)
    delete originDb[index] 
    originDb = originDb.filter(x => x != null)   
    fs.writeFile('database.json', JSON.stringify(originDb), 'utf8',
        function(err) {
          res.status(201).json({result:true,list : originDb})
        })
  })
});

router.get('/', function(req, res, next) {
  fs.readFile('database.json', 'utf8', function(err, data) {                
    let originDb = JSON.parse(data)
    res.status(200).json({result:true,list : originDb})
  })
})

module.exports = router;
