module.exports = {
    database: {
      host: 'prod_url',
      user: "root",
      password: "12341234",
      database: "sys",
      port: "3306",
      connectionLimit: "10", // pool에 담을 수 있는 최대 connection 개수
      timezone: "utc",
      debug: ['ComQueryPacket', 'RowDataPacket']
    }
}
