module.exports = {
    database: {
      host: '127.0.0.1',
      user: "root",
      password: "12341234",
      database: "sys",
      port: "3306",
      connectionLimit: "10", // pool에 담을 수 있는 최대 connection 개수
      timezone: "utc",
      debug: ['ComQueryPacket', 'RowDataPacket']
    }
}

// 분기처리 (local, prod, dev)
// 맞는 값을 가져와서 반환