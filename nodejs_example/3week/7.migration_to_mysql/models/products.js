// module.exports.getList = (connection, options) => {
//     return new Promise((resolve, reject) => {
//         connection.query('SELECT * FROM Products',  function (error, results, fields) {
//             if (error) {
//                 // res.status(200).json({result:false})
//                 reject(error)
//                     // throw error;                
//             }                   
//             connection.commit(function(err) {
//                 if (err) {
//                     reject(err)
//                     // res.status(200).json({result:false})
//                     // throw err;                    
//                 }
//                 console.log('success! : ',results);
//                 // return results;
//                 resolve(results);
//             });                
//         });
//     });                
// };

const db = require('../components/db')

module.exports.getList = async (options) => {
    console.log('options : ',options)
    let query = 'SELECT * FROM Products '
    let values = []
    if(options.idx){
        query += ` WHERE idx = ${options.idx}`
        values.push(options.idx)
    }


    return await db.query({
        query: query,
        value: values
    })        
};

module.exports.insert = async (options, connection) => {
    console.log('options : ',options)
    return await db.query({
        connection:connection,
        query: 'INSERT INTO Products SET ?',
        value: options
    })  
};

module.exports.update = async (options, connection) => {
    console.log('update options : ',options)
    return await db.query({
        connection:connection,
        query: 'UPDATE Products SET ? WHERE idx = ?',
        value: [options, options.idx]
    })  
};

module.exports.delete = async (options, connection) => {
    console.log('options : ',options)
    return await db.query({
        connection:connection,
        query: 'DELETE FROM Products WHERE idx = ?',
        value: [options.idx]
    })  
};