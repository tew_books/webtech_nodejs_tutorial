var express = require('express');
var router = express.Router();

const config = require('../config')
const mysql = require('mysql');

const pool  = mysql.createPool({
    connectionLimit : config.database.connectionLimit,
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database
});

router.post('/', function (req, res, next) {
    const body = req.body; // {name:asdf,price:200}
    console.log('body : ', body)
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        connection.beginTransaction(function(err) {
            if (err) { 
                res.status(200).json({error:err})                
            }            
            connection.query(
                'INSERT INTO products SET ?', 
                body,
                function (error, result, fields) {
                    if (error) {
                        return connection.rollback(function() {                                    
                            res.status(200).json({error:error})                            
                        });
                    }
                    connection.commit(function(err) {
                        connection.release();
                        if (err) {
                            return connection.rollback(function() {
                              res.status(200).json({err:err})                                
                            });
                        }
                        res.status(200).json({result})
                    });                       
            })
            
        })
    })
})


router.put('/', function (req, res) {
    const body = req.body  // { "idx":3,"name":"changed","price":12320000}
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        connection.beginTransaction(function(err) {
            if (err) { 
                res.status(200).json({error:err})                
            }
            connection.query(
                'UPDATE products SET ? WHERE idx = ?', 
                [body, body.idx], 
                function (error, result, fields) {
                    if (error) {
                        connection.release();
                        return connection.rollback(function() {
                            res.status(200).json({error:error})                            
                        });
                    }
                    connection.commit(function(err) {
                        connection.release();
                        if (err) {
                            return connection.rollback(function() {
                              res.status(200).json({error:err})                                
                            });
                        }
                        res.status(200).json({result})
                    });   
                }
            )
        })
    })    
})

router.delete('/', function (req, res) {
    const body = req.body  
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
        connection.beginTransaction(function(err) {
            if (err) { 
                res.status(200).json({error:err})                
            }
            connection.query(
                'DELETE FROM products WHERE idx = ?', 
                body.idx, 
                function (error, result, fields) {
                    if (error) {
                        connection.release();
                        return connection.rollback(function() {
                            res.status(200).json({error:error})                            
                        });
                    }
                    connection.commit(function(err) {
                        connection.release();
                        if (err) {
                            return connection.rollback(function() {
                              res.status(200).json({error:err})                                
                            });
                        }
                        res.status(200).json({result})
                    });   
                }
            )
        })
    })    
    
})

router.get('/', function (req, res) {
    const {idx} = req.query // localhost:3000/products?idx=4
    pool.getConnection(function(err, connection) {
        connection.query(
            'SELECT * FROM products WHERE idx = ?',  // SELECT * FROM products
            idx,  
            function (error, result, fields) {
                if (error) {
                    res.status(404).json({result:false})
                        // throw error;                
                }          
                res.status(200).json({result})                                        
            }
        )
    })   

})




module.exports = router;