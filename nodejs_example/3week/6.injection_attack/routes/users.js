var express = require('express');
var router = express.Router();

/* GET users listing. */
// 127.0.0.1:3000/users/abcd
router.get('/abcd', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
