var express = require('express');
var router = express.Router();

const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234'
});
const pool  = mysql.createPool({
  connectionLimit : 10,
  host            : '127.0.0.1',
  user            : 'root',
  password        : '12341234',
  database        : 'sys'
});


router.get('/', function(req, res, next) {
  // body data : req.body
  // 주소 (query string) : req.query
  // 호출 : url?idx=1
  // 호출 : localhost:3000?idx=1&name=asdf
  
  let query = req.query // {idx:1, name:asdf}
  console.log('query : ',query)
  // query = {idx:1}

  // idx를 가져오는 방법
  // let idx = query.idx // 1 / req.query.idx
  // let name = query.name
  // let addres = query.address  
  // console.log('idx : ', idx) // 출력 idx : 1
  let {idx, name} = query // 1
  console.log('idx : ', idx) // 출력 idx : 1
  console.log('name : ', name) // 출력 name : asdf
  

  // let {idx} = req.query
  // console.log('idx :', idx)
  // 호출 : 127.0.0.1:3000?idx=1
  // detail get : url/products/{idx}
  // detail get : url/products
 
  let sql;
  // sql = 'SELECT * FROM products WHERE products_idx = "'+idx+'"'
  

  //위는 인젝션 공격에 취약한 형태
  // sql : SELECT * FROM products WHERE idx = "6"
  // 예상하는 호출 : localhost:3000?idx=1
  // 공격 : localhost:3000?idx=whatever" or 1="1 => 모든 데이터를 가져오게 됨
  // 적용 된 sql : SELECT * FROM products WHERE idx = "whatever" or 1="1"


  // 위 형태에서는 인젝션 공격을 피하기 위해서 다음과 같이 사용해야 합니다.
  // pool.getConnection(function(err, connection){
  //   sql = 'SELECT * FROM products WHERE products_idx = ' + connection.escape(idx);
  //   console.log('sql : ', sql)
  //   // 공격 : localhost:3000?idx=whatever" or 1="1 => 실패
  //   connection.query(sql, function (error, results, fields) {
  //     if (error) {
  //       console.log('error : ', error)
  //       throw error;
  //     }
  //     // Neat!
  //     console.log('results : ', results)
  //   });
  
  // })
  
  


  // 권장되는 방식
  // const product = req.body
  // pool.query('INSERT INTO products SET ?', product, function (error, results, fields) {
  //   if (error) throw error;
  //   // Neat!
  // });

  // let columns = ['name'] // 1개일때는 array 아니어도 됨
  // pool.query('SELECT ?? FROM ?? WHERE idx = ?', 
  //   [columns, 'products', idx], 
  //   function (error, results, fields) {
  //   if (error) {
  //     console.log('error : ', error)
  //   };
  //   // ...
  //   res.json({results})
  // });


  
  
  // Rollback 예제
  // pool.getConnection으로 connection을 가져옴
  let {name} = req.body
  pool.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.beginTransaction(function(err) {
      if (err) { throw err; }

      connection.query('INSERT INTO products SET name=?', name, function (error, results, fields) {
          if (error) {
              return connection.rollback(function() {
                  throw error;
              });
          }
          console.log('products results ',results)
          var log = 'Post ' + results.insertId + ' added';
          connection.query('INSERT INTO log SET data=?', log, function (error, results, fields) { 
          if (error) {
              return connection.rollback(function() { // table이 없어서 error 발생하여 롤백
                  throw error;
              });
          }
              connection.commit(function(err) {
                  if (err) {
                  return connection.rollback(function() {
                      throw err;
                  });
                  }
                  console.log('success!');
              });
          });
      });
  });


})
  


  // res.render('index', { title: 'Express' });
});



module.exports = router;
