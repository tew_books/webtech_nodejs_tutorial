var express = require('express');
var router = express.Router();

const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234'
});
const pool  = mysql.createPool({
  connectionLimit : 10,
  host     : '127.0.0.1',
  user     : 'root',
  password : '12341234',
  database : 'sys'
});



router.post('/', function(req, res, next) {
    console.log('req : ', req.body)
    res.render('index', { title: 'Express' });
});


router.put('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.delete('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/', function(req, res, next) {

  // mysql db 연결 1
  // connection.connect(function(err) {
  //   if (err) {
  //       console.error('error connecting: ' + err.stack);
  //       return
  //   }
  //   console.log('connected as id ' + connection.threadId);
  // });

  // 연결을 하지 않고 바로 쿼리문을 실행할 수도 있습니다.
  // connection.query('SELECT 1', function (error, results, fields) {
  //   if (error) throw error;
  //   console.log('connected')
  // // connected!
  // });

  // pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
  //   if (error) throw error;
  //   console.log('The solution is: ', results[0].solution);
  // });
  
  
  // 위 함수의 경우, connection을 새로 열 수 있는 버그가 있습니다. (NodeJS MySQL Docu)
  // 때문에 아래와 같이 작업하는 걸 권유 합니다.
  pool.getConnection(function(err, connection) {
    if (err) throw err; // not connected!

        // Use the connection
        connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            console.log('getConnection')
            // Handle error after the release.
            if (error) throw error;

            // Don't use the connection here, it has been returned to the pool.
        });
    });

  res.render('index', { title: 'Express' });
});



module.exports = router;
