var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Change Title' });
});


// router.get('/about', function (req, res) {
//   res.send('about')
// })

// 익스프레스 라우팅은 패턴을 감지 합니다.
// 아래 코드는 acd 혹은 abcd를 라우팅 할 것입니다.
// router.get('/ab?cd', function (req, res) {
//   res.send('ab?cd')
// })

//아래 코드는 abcd, abbcd, abbbcd 등을 라우팅합니다.
// router.get('/ab+cd', function (req, res) {
//   res.send('ab+cd')
// })

//아래 코드는 abcd, abxcd, abRANDOMcd 등을 라우팅합니다.
// router.get('/ab*cd', function (req, res) {
//   res.send('ab*cd')
// })

//아래 코드는 d가 포함 돼어 있는 경로를 라우팅 합니다.
// router.get(/d/, function (req, res) {
//     res.send('/d/')
// })

//butterfly등 fly앞에 붙어있는 경로를 합쳐서 라우팅 합니다.
// router.get(/.*fly$/, function (req, res) {
//     res.send('/.*fly$/')
// })

//경로에 파라미터를 추가할 수도 있습니다.
// localhost:3000/users/testId
router.get('/users/:userId', function (req, res) {
    console.log('users req.params : ',req.params)
    res.send(req.params)
})

// - 와  . 으로도 구분이 가능합니다.
// endpoint/fligts?from=SEOUL&to=NYC
router.get('/flights/:from-:to', function (req, res) {
  // localhost:3000/flights/LAX-SFO
  const {from, to} = req.params
  res.send(req.params)
})

router.get('/plantae/:genus.:species', function (req, res) {
  // plantae/Prunus.persica
  res.send(req.params)
})

// multi callback
router.get('/example/b', function (req, res, next) {
  console.log('the response will be sent by the next function ...')
  next() // 다음 작업을 실행한다.
  
}, function (req, res) {
  console.log('second')
  res.send('Hello from B!')
})



// multi callback 2
var cb0 = function (req, res, next) {
  console.log('CB0')
  next()
}

var cb1 = function (req, res, next) {
  console.log('CB1')
  next()
}

var cb2 = function (req, res) {
  res.send('Hello from C!')
}

router.get('/example/c', [cb0, cb1, cb2])

module.exports = router;


// multi callback 3 

var cb0 = function (req, res, next) {
  console.log('CB0')
  next()
}

var cb1 = function (req, res, next) {
  console.log('CB1')
  next()
}

router.get('/example/d', [cb0, cb1], function (req, res, next) {
  console.log('the response will be sent by the next function ...')
  next()
}, function (req, res) {
  res.send('Hello from D!')
})