
// 콜백 함수에 대한 제대로 된 설명은 진도가 안나갔지만 우선 아래 예제 참고하실 수 있습니다.
// 우리가 반복적으로 사용하는 코드를 별도로 작성하여 관리하고, 이를 필요시마다 불러오기로 했습니다.
// 혹은 외부 모듈을 설치해서 쓰는데 그 모듈의 함수를 불러오는 경우도 있겠습니다.
// 우리는 해당 함수의 계산은 모르지만, 
// 이 함수는 매개변수로 입력값을 주면 결과를 함수로 전달합니다.
// 즉 입력정보와 결과를 받는 함수를 매개변수로 전달하게 됩니다.
// 이때 실행함수의 결과를 전달하는 함수를 콜백함수라고 합니다.

// 우리는 아래 함수를 사용할 예정입니다.
function sumFunction(num1, num2, callback){
    // num1과 num2를 매개변수로 받아서 더합니다.
    let result = num1+num2
    if('number' == typeof(result)){
        // 결과가 숫자일 경우, 결과를 callback 함수의 매개변수로 전달합니다.
        let error
        let message = result
        callback(error, message)
    } else {
        // 결과가 숫자가 아닐 경우, 에러를 callback 함수의 매개변수로 전달합니다.
        let error = 'is Not Number : ', result
        let message 
        callback(error, message)
    }
}

// 위 함수를 실행 시 매개변수로 입력값, 콜백함수를 전달 합니다.
sumFunction(1, 2, function(error, message){
    // 위 함수에서 내부 함수가 실행된 결과를 error와 message라는 매개함수로 전달받습니다.
    console.log("error : ",error)
    console.log("message : ",message)
})






// callFunction()
