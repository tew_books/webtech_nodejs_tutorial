let jsonObject = {
    "name": "dahan",
    "gender": "M",
    "hobbies":[
        {
            "name":"soccer",
            "level":1
        },
        {
            "name":"base ball",
            "level":2
        }
    ]
}



console.log('jsonObject : ',jsonObject)
console.log('typeof(jsonObject) : ',typeof(jsonObject))
let stJson = JSON.stringify(jsonObject)
console.log('stJson : ',stJson.name)
console.log('typeof(stJson) : ',typeof(stJson))
let jsonObjectAgain = JSON.parse(stJson)
console.log('jsonObjectAgain : ',jsonObjectAgain)
console.log('jsonObjectAgain.name : ',jsonObjectAgain.name)
// jsonObjectAgain.gender = "man"
// console.log('jsonObjectAgain["gender"] : ',jsonObjectAgain["gender"])

// console.log('jsonObjectAgain.gender : ',jsonObjectAgain.gender)